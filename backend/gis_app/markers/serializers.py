from rest_framework import serializers
from drf_extra_fields.geo_fields import PointField

from markers.models import Marker


class InMarkerSerializer(serializers.ModelSerializer):
    point = PointField()

    class Meta:
        model = Marker
        fields = ('id', 'point', 'description')


class OutMarkerSerializer(serializers.ModelSerializer):
    position = serializers.SerializerMethodField()
    draggable = serializers.SerializerMethodField()

    class Meta:
        model = Marker
        fields = ('id', 'position', 'draggable', 'description')
    
    def get_position(self, instance):
        return [instance.point.x, instance.point.y]

    def get_draggable(self, obj):
        return self.context['request'].user == obj.owner
