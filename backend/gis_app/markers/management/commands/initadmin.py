from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
from rest_framework.authtoken.models import Token, TokenProxy



User = get_user_model()


User = get_user_model()
class Command(BaseCommand):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            username = 'admin@example.com'
            email = 'admin@example.com'
            password = 'admin'
            print('Creating account for %s (%s)' % (username, email))
            admin = User.objects.create_superuser(email=email, username=username, password=password)
            admin.is_active = True
            admin.is_admin = True
            admin.save()
            token=Token(user=admin)
            token.save()

            print('The token for {}: {}'.format(username, TokenProxy.objects.get(user=admin).key))
        else:
            print('Admin accounts can only be initialized if no Accounts exist')
