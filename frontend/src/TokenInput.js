import React, { useState } from "react";


const TokenInput = (props) => {
    const [token, setToken] = useState(localStorage.getItem('token') || '')

    const saveToken = () => {
        localStorage.setItem('token', token)
        setToken(token)
        props.shareToken(token)
    }

    return (
      <div>
        <input onChange={(e) => setToken(e.target.value)}/>
        <button onClick={saveToken}>Save Token</button>
      </div>
    )
  }

  export default TokenInput