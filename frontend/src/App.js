import MapComponent from "./MapComponent";
import TokenInput from "./TokenInput";
import { useEffect, useState } from "react";


function App() {
  const [token, setToken] = useState('')

  useEffect(() => {
    if (token === '') {
      const token = localStorage.getItem('token')
      setToken(token)
    }
  }, [token]);

  const getToken = (token) => {
    fetch('http://127.0.0.1:8000/api/', { 
        method: 'GET', 
        headers: new Headers({
          'Authorization': 'Token ' + token, 
          'Content-Type': 'application/json'
        }), 
      })
      .then(response => {
        if (response.status === 200) {
          setToken(token)
        } else {
          localStorage.removeItem('token')
        }
      })
  }

  if (token) {
    return (
      <div className="App">
        <MapComponent token={token} /> 
      </div>)
  } else {
    return (
      <div className="App">
        <TokenInput shareToken={getToken}/>
      </div>)
  }
}

export default App;
