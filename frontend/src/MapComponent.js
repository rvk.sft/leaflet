import React, { useState, useEffect, useMemo } from "react";
import { Fragment } from "react";
import { MapContainer, TileLayer, Popup, Marker, useMapEvents } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from 'leaflet';

import imageLocationURL from './marker-icon.png';
import imageFLocationURL from './marker-icon-red.png';


const BlueIcon = L.icon({
  iconUrl: imageLocationURL,
  iconSize: [15, 25]
});

const RedIcon = L.icon({
  iconUrl: imageFLocationURL,
  iconSize: [15, 25]
});

const DraggableMarker = ({ center, api_id, draggable }) => {
    const eventHandlers = useMemo(
      () => ({
        dragend(e) {
          // const marker = markerRef.current
          fetch('http://127.0.0.1:8000/api/' + e.target.options.api_id + '/', {
            method: 'PATCH', 
            headers: new Headers({
              'Authorization': 'Token bb739a64b1d319b5fe28cbd67252f4c9934becba', 
              'Content-Type': 'application/json'
            }),         
            body: JSON.stringify(
              {
                point: {
                  "latitude": e.target._latlng.lng,
                  "longitude": e.target._latlng.lat
                },
              }
          )})
        }
      }),
      [],
    )

    const icon = draggable === true || undefined ? BlueIcon : RedIcon

    return (
      <Marker
        icon={icon}
        draggable={draggable}
        eventHandlers={eventHandlers}
        position={center}
        api_id={api_id}
        >

      </Marker>
    )
  }
  

const MyPopupMarker = ({ position, description, id, draggable }) => (
    <DraggableMarker center={position} radius={10} content={description} api_id={id} draggable={draggable}>
       <Popup>{description}</Popup>
    </DraggableMarker>
  )
  
const MyMarkersList = ({ markers }) => {
  if (markers.length > 0) {
    const items = markers.map((item, id) => <MyPopupMarker key={id} {...item} />)
    return <Fragment>{items}</Fragment>
  }
    else {
      return ''
    }
}

const MarkersComponent = (props) => {
  useMapEvents({
    click: (e) => {
      const new_marker = { position: [ e.latlng.lat, e.latlng.lng], content: '1', draggable: true }

      fetch('http://127.0.0.1:8000/api/', {
        method: 'POST', 
        headers: new Headers({
          'Authorization': 'Token ' + props.token, 
          'Content-Type': 'application/json'
        }),         
        body: JSON.stringify(
          {
            point: {
              "latitude": new_marker.position[1],
              "longitude": new_marker.position[0]
            },
            description: new_marker.content
          }
          )})
          .then(data => data.json())
          .then(response => {
            new_marker.id = response.id
            const new_markers = [
              ...props.markers,
              new_marker
            ];
            props.onClick(new_markers)
          })
    },
  })
  return null
}


const MapComponent = (token) => {
    const example_markers = []

    const [markers, setMarkers] = useState(example_markers)

    useEffect(() => {
      fetch('http://127.0.0.1:8000/api/', { 
        method: 'GET', 
        headers: new Headers({
          'Authorization': 'Token ' + token['token'], 
          'Content-Type': 'application/json'
        }), 
      })
        .then(data => data.json())
        .then(items => {
            setMarkers(items)
        })
    }, [token])

    
      return (
          <MapContainer 
            style={{ height: "90vh" }} zoom={8} center={[51.5, 0]} >
              <MarkersComponent onClick={setMarkers} markers={markers} token={token['token']} />
              <MyMarkersList markers={markers} />
              <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          </MapContainer>
      )
};

export default MapComponent;