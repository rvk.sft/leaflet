from markers.models import Marker

from rest_framework import viewsets
from rest_framework.viewsets import mixins, GenericViewSet

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from markers.models import Marker
from markers.serializers import InMarkerSerializer, OutMarkerSerializer
from markers.permissions import UpdateMarkerPermission


class MarkersViewSet(mixins.CreateModelMixin, 
                   mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, UpdateMarkerPermission]

    queryset = Marker.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return OutMarkerSerializer
        return InMarkerSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
