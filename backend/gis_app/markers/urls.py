from rest_framework.routers import DefaultRouter
from django.urls import path, include

from markers.views import MarkersViewSet

router = DefaultRouter()
router.register(r'api', MarkersViewSet, basename='api')

app_name = 'markers'

urlpatterns = [
    path(r'', include(router.urls)),
]