from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
from rest_framework.authtoken.models import Token, TokenProxy

User = get_user_model()


User = get_user_model()
class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            username = 'usualuser@example.com'
            email = 'usualuser@example.com'
            password = 'admin'
            print('Creating account for %s (%s)' % (username, email))
            user = User(email=email, username=username, password=password)
            user.is_active = True
            user.save()
            
            token=Token(user=user)
            token.save()

            print('The token for {}: {}'.format(username, TokenProxy.objects.get(user=user).key))
        except Exception as e:
            print("User exist")

