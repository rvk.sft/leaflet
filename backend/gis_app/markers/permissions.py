from rest_framework import permissions
from markers.models import Marker


class UpdateMarkerPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if view.action == 'partial_update' or view.action == 'update':
            if request.user.is_superuser:
                return True
            try:
                marker = Marker.objects.get(pk=view.kwargs.get('pk'))
                return marker.owner == request.user
            except Exception as e:
                return False
        else:
            return True
