'''
This is simple example of django tests.
'''
import uuid
import factory

from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.reverse import reverse
from django.test import RequestFactory
from faker.providers import BaseProvider
from django.contrib.gis.geos import Point
from django.contrib.auth.models import User

from markers.models import Marker

class DjangoGeoLocationProvider(BaseProvider):

    def geo_point(self, **kwargs):
        kwargs['coords_only'] = True
        faker = factory.faker.faker.Faker()
        coords = faker.local_latlng(**kwargs)
        return Point(x=float(coords[1]), y=float(coords[0]), srid=4326)


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'user%s@example.com' % n)
    email = factory.Sequence(lambda n: 'user%s@example.com' % n)
    password = factory.PostGenerationMethodCall('set_password', 'default')
    is_staff = False


class MarkerFactory(factory.django.DjangoModelFactory):
    factory.Faker.add_provider(DjangoGeoLocationProvider)

    class Meta:
        model = Marker

    point = factory.Faker('geo_point', country_code='US')
    description = factory.Sequence(lambda n: 'Unique description %s' % str(uuid.uuid4()))
    owner = factory.SubFactory(UserFactory)


class MarkerListAPITestCase(APITestCase):
    def setUp(self):
        self.user = UserFactory(is_staff=True)
        self.client.force_authenticate(user=self.user)
        self.url = reverse('markers:api-list')

        self.marker_1 = MarkerFactory()
        self.marker_2 = MarkerFactory()
        
        request = RequestFactory().get('/')
        request.user = self.user
        self.context = {'request': request}

    def test_url(self):
        self.assertEqual(self.url, '/api/')

    def test_get_markers_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)


        