from django.contrib.gis.db import models as geomodels
from django.conf import settings
from django.db import models


class Marker(geomodels.Model):
    point = geomodels.PointField()
    description = models.CharField(max_length=100)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=False)

    def __str__(self):
        return "Marker(lat={} and lng={} by {}".format(self.point.x, self.point.y, self.owner)
