from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from markers.models import Marker


User = get_user_model()


class MarkerAdmin(admin.ModelAdmin):
    list_filter = ('owner', )
    
    def get_list_display_links(self, request, list_display):
        super().get_list_display_links(request, list_display)
        return None
    
    def has_add_permission(self, request, obj=None):
        return False


class CustomUserAdmin(UserAdmin):
    def __init__(self, *args, **kwargs):
        super(UserAdmin,self).__init__(*args, **kwargs)
        UserAdmin.list_display = list(UserAdmin.list_display) + ['date_joined', 'markers_count']

    def markers_count(self, obj):
        return Marker.objects.filter(owner=obj).count()


admin.site.register(Marker, MarkerAdmin)
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
